export WORKDIR=$(pwd)

echo "*** REMOVING OLD CONTAINER ***"
docker rm -f  botnoi-school-auth


echo "\n############### DEPLOYING ###############"
docker-compose -f ${WORKDIR}/botnoi-school-auth/docker-compose.yml pull && \
docker-compose -f ${WORKDIR}/botnoi-school-auth/docker-compose.yml up -d

echo "\n*** REMOVING OLD IMAGES ***"
docker rmi $(docker images | grep '<none>' | awk {'print$3'})

echo "\n*** END PROCESS ***"